const apiUrl = "https://reqres.in"

export default {
  getUsers(page) {
    return fetch(`${apiUrl}/api/users?page=${page}`)
  },
  getUserDetail(id) {
    return fetch(`${apiUrl}/api/users/${id}?delay=3`)
  },
  removeUser(id) {
    return fetch(`${apiUrl}/api/users/${id}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
      'Content-Type': 'application/json',
      }
    })
  },
  addUser(body) {
    return fetch(`${apiUrl}/api/users`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
  },
  updateUser(id, body) {
    return fetch(`${apiUrl}/api/users/${id}`, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
      'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
  },
  register(body) {
    return fetch(`${apiUrl}/api/register`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
  },
  login(body) {
    return fetch(`${apiUrl}/api/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
  }
}
