import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Users from './components/Users';
import Register from './components/Register';
import Login from './components/Login';

import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <header className="users-header">
          <h1 className="users-title">Api consumption test</h1>
        </header>
        <Switch>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
          <Route path="/users" component={Users}/>
          <Redirect from="" to="/login" />
        </Switch>
      </div>
    );
  }
}

export default App;
