import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Api from '../Api';
import Notifications from './Notification';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      isLoading: false,
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleClick = (event) => {
    const body = {
      email: this.state.email,
      password: this.state.password
    }
    Api.login(body)
    .then(res => res.json())
    .then(result => {
      if (result.error) {
        this.setState({error: result.error})
      } else if (result.token) {
        this.context.router.history.push('/users', { token: result.token})
      }
    }, error => this.setState({ isLoading: false, error })
    )
  }
  render() {
    const { email, password, error } = this.state
    return (
      <div className="login-container">
        <h2>User registration</h2>
        <div className="form">
          <label>
            Email
            <input onChange={this.handleChange} value={email} name="email" type="text" />
          </label>
          <label>
            Password
            <input onChange={this.handleChange} value={password} name="password" type="password"/>
          </label>
          <div className="buttons">
            <button><Link to="/register">Register</Link></button>
            <button onClick={this.handleClick}>Login</button>
          </div>
        </div>
        {error && Notifications(error)}
      </div>
    );
  }
}

export default Login;

Login.contextTypes = {
  router: PropTypes.object.isRequired
};