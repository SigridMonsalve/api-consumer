import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Api from '../Api';
import Notifications from './Notification';
import UserDetail from './UserDetail';
import logo from '../logo.svg';

class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      isLoading: false,
      user: {
        first_name: '',
        last_name: '',
        avatar: ''
      },
      pagination: {},
      error: '',
    }
    this.navigation = this.props.history;
    this.route = this.props.location
  }

  componentWillMount() {
    if (!this.route.state.token) {
      this.navigation.push('/login')
    }
    this.setState({ isLoadingList: true });
    this.getUsers()
  }

  getUsers = (page = 1) => {
    Api.getUsers(page)
      .then(res => res.json())// res.status
      .then(result => {
        const { page, per_page, total, total_pages } = result;
        this.setState({
          isLoadingList: false,
          items: result.data,
          pagination: { page, per_page, total, total_pages},
        });
      }, error => this.setState({ isLoadingList: false, error })
    )
  }
  onUserSelect = (id) => {
    if(this.state.isLoadingUser) {
      return
    }
    this.setState({isLoadingUser: true});
    Api.getUserDetail(id)
    .then(res => res.json())
    .then(result => {
      this.setState({
        isLoadingUser: false,
        user: {
          first_name: result.data.first_name,
          last_name: result.data.last_name,
          avatar: result.data.avatar,
          id: result.data.id
        }
      });
    }, error => this.setState({ isLoadingUser: false, error })
    )
  }
  onAdd = () => {
    const body = {
      first_name: 'Sigrid',
      last_name: 'Monsalve', 
    }
    Api.addUser(body)
    .then(res => {
      if (res.status === 201) {
        return res.json()
      } else {
        return { error: 'User could not be added successfully' }
      }
    })
    .then(res => {
      if (res.error) {
        this.setState({ error: res.error})
      } else {
        this.setState({ error: `New user ${res.first_name} was added successfully at ${res.createdAt.slice(0, 10)}`})
      }
    }, error => this.setState({ isLoading: false, error })
    )
  }
  onRandomSelect = () => {
    const unknownUser = Math.floor(Math.random() * 100) + 13;
    Api.getUserDetail(unknownUser)
    .then(res => {
      if (res.status === 404) {
        this.setState({ error: `Random user with id ${unknownUser} not found`})
      }
      return res.json()})
    .then(result => {
    }, error => this.setState({ isLoading: false, error })
    )
  }
  onRemoveUser = (id) => {
    Api.removeUser(id)
    .then(res => {
      if (res.status === 204) {
        this.setState({
          error: `User with id ${id} has been removed`,
          user: {first_name: '', last_name: '', id: '', avatar: ''}
        })
      }
      return
    }, error => this.setState({ isLoading: false, error })
    )
  }
  onUpdateUser = (id, body) => {
    Api.updateUser(id, body)
    .then(res => res.json())
    .then(result => {
      if (result.error) {
        this.setState({error: result.error})
      } else {
        this.setState({error: `User with id ${id} has been updated at ${result.updatedAt.slice(0, 10)}`})
      }
    }, error => this.setState({ isLoading: false, error })
    )
  }
  render() {
    const userList = this.state.items.map((item, id) => (
      <li className="list-item" onClick={() => this.onUserSelect(item.id)} key={id}>
        <img src={item.avatar} alt="mini-avatar" />
        <p>{item.first_name} {item.last_name}</p>
      </li>
      )
    )
    const { user, isLoadingUser, isLoadingList, pagination, error } = this.state
    return (
      <div className="Users">
        <div className="userlist-container">
          <h2>Users list</h2>
          <div className="content">
            <div>
              <p>Displaying page {pagination.page} of {pagination.total_pages}</p>
              <div className="buttons">
                <button disabled={pagination.page === 1} onClick={() => this.getUsers(pagination.page - 1)}>
                  Prev
                </button>
                <button
                  disabled={pagination.page === pagination.total_pages}
                  onClick={() => this.getUsers(pagination.page + 1)}>
                  Next
                </button>
              </div>
            </div>
            <div>
              <ul>
                {isLoadingList ? <img src={logo} className="users-logo" alt="logo" /> : userList}
              </ul>
              <div className="buttons">
                <button onClick={this.onAdd}>Add Me</button>
              </div>
            </div>
          </div>
        </div>
        <div className="detail-container">
          <h2>Selected user</h2>
          <div className="buttons">
            <button onClick={this.onRandomSelect}>Select Random</button>
          </div>
          {isLoadingUser 
            ? <img src={logo} className="users-logo" alt="logo" />
            : user.avatar !== '' 
            ? <UserDetail user={user} remove={this.onRemoveUser} update={this.onUpdateUser}/>
            : <p>No user selected</p>
          }
        </div>
        {error !== '' && Notifications(error)}
      </div>
    );
  }
}

export default Users;

Users.contextTypes = {
  router: PropTypes.object.isRequired
};
