import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import Api from '../Api';
import Notifications from './Notification';

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      emailConfirmation: '',
      password: '',
      passwordConfirmation: '',
      isLoading: false,
    }
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleClick = (event) => {
    const body = {
      email: this.state.email,
      username: this.state.email,
      password: this.state.password
    }
    Api.register(body)
    .then(res => res.json())
    .then(result => {
      if (result.error) {
        this.setState({error: result.error})
      } else if (result.token) {
        this.context.router.history.push('/users', { token: result.token})
      }
    }, error => this.setState({ isLoading: false, error })
    )
  }
  render() {
    const { email, emailConfirmation, password, passwordConfirmation, error } = this.state
    return (
      <div className="register-container">
        <h2>User registration</h2>
        <div className="form">
          <label>Email
            <input onChange={this.handleChange} value={email} name="email" type="text" />
          </label>
          <label>Repeat your email
            <input onChange={this.handleChange} value={emailConfirmation} name="emailConfirmation" type="text"/>
          </label>
          <label>Password
            <input onChange={this.handleChange} value={password} name="password" type="password"/>
          </label>
          <label>Repeat your password
            <input onChange={this.handleChange} value={passwordConfirmation} name="passwordConfirmation" type="password"/>
          </label>
          <div className="buttons">
            <button><Link to="/login">Login</Link></button>
            <button onClick={this.handleClick}>Register</button>
          </div>
        </div>
        {error && Notifications(error)}
      </div>
    );
  }
}

export default Register;

Register.contextTypes = {
  router: PropTypes.object.isRequired
};