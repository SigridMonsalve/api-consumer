import React, { Component } from 'react';
import Api from '../Api';

class UserDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editing: false,
      first_name: this.props.user.first_name,
      last_name: this.props.user.last_name,
      id: this.props.user.id
    }
  }
  onRemove = () => {
    this.props.remove(this.props.user.id)
  }
  onEdit = () => {
    this.setState({editing: !this.state.editing})
  }
  onUpdate = () => {
    const { id, first_name, last_name } = this.state
    this.props.update(id, { first_name, last_name })
  }
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }
  render() {
    const { editing, first_name, last_name } = this.state
    return (
      <div>
        <div className="list-item user-detail">
          <img src={this.props.user.avatar} className="user-avatar" alt="logo" />
          <div className="form">
            <label>First name
              <input disabled={!editing} className={!editing ? 'disabled' : ''} onChange={this.handleChange} value={first_name} name="first_name" type="text" />
            </label>
            <label>Last name
              <input disabled={!editing} className={!editing ? 'disabled' : ''} onChange={this.handleChange} value={last_name} name="last_name" type="text"/>
            </label>
          </div>
          <div className="buttons">
            <button onClick={this.onRemove}>Remove user</button>
            <button onClick={editing ? this.onUpdate : this.onEdit}>{editing ? 'Update' : 'Edit'} user</button>
          </div>
        </div>
      </div>
    );
  }
}

export default UserDetail;